<?php namespace RemoveTribeEventsSchema
{
	/*
	Plugin Name:  Remove Tribe Events Schema
	Plugin URI:   https://www.jaimeson-waugh.com
	Description:  Removes schema automatically added by Tribe Events Calendar Plugin
	Version:      1.0
	Author:       Jaimeson Waugh
	Author URI:   https://www.jaimeson-waugh.com
	License:      GPL2
	License URI:  https://www.gnu.org/licenses/gpl-2.0.html
	Text Domain:  remove-tribe-events-schema
	Domain Path:  /languages

	"Remove Tribe Events Schema" is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	any later version.

	"Remove Tribe Events Schema" is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with "Remove Tribe Events Schema". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
	*/

	function RemoveTribeEventsSchema( $html )
	{
		return '';
	};
	add_filter( 'tribe_json_ld_markup', '\RemoveTribeEventsSchema\RemoveTribeEventsSchema' );
}
